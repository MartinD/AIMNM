/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Miquel and Martin and Nathan
 */
public class GeneticAlgorithm {

    private ArrayList<Entity> entities;
    private double prop = 0.5;
    
    public GeneticAlgorithm(ArrayList<Entity> entities){
        this.entities = entities;
    }

    private TupleList getScore(){
        TupleList scores = new TupleList();
        int score;
        for(Entity e : entities){
            score = (int) (200*Math.pow(e.getEnergy(),3)+(int)Math.sqrt(Math.abs(e.getCredit())));
            scores.add(new Tuple(e,score));
        }
        return scores;
    }


    private ArrayList<Entity> getBadEntities(){
        TupleList scores = getScore();
        ArrayList<Entity> badEntities = new ArrayList<>();
        int size = (int)(scores.size()*prop);
        for(int i = 0; i<size; i++){
            badEntities.add(scores.get(i).getEnt());
        }
        return badEntities;
    }

    private void crossOver(){
        ArrayList<Entity> badEntities = getBadEntities();
        for(int i=0; i<badEntities.size()-1; i+=2){
            ArrayList<Matrix> weights1 = badEntities.get(i).getNeuronNet().getWeights();
            ArrayList<Matrix> weights2 = badEntities.get(i+1).getNeuronNet().getWeights();
            for(int j=0; j<weights1.size(); j++){
                Matrix.mixMatrices(weights1.get(j), weights2.get(j));
            }
        }
    }

    public void mutation(){
        for(Entity e : entities){
            ArrayList<Matrix> weights = e.getNeuronNet().getWeights();
            for(Matrix matrix : weights){
                flip(matrix);
            }
            e.getNeuronNet().setWeights(weights);
        }
    }

    private float randRange(float min, float max) {
        return min + (float)Math.random() * (max - min);
    }
    
    private void flip(Matrix matrix){
        boolean isFlipped = randRange(0,1) >= 0.95;
        if(isFlipped){
            int randomRow = (int) randRange(0, matrix.getRows());
            int randomCol = (int) randRange(0, matrix.getColumns());
            matrix.setValue(randomRow, randomCol, randRange(-4,4));
        }
    }

    public void nextGeneration(){
        crossOver();
        mutation();
        for(Entity e : entities){
            e.setCredit(e.getMaxCredit());
            e.setEnergy(100);
        } 
    }

    public ArrayList<Entity> getEntities(){
        return entities;
    }

    public void setEntities(ArrayList<Entity> entities) {
        this.entities = entities;
    }

    public void setProp(double prop) {
        this.prop = prop;
    }
}
