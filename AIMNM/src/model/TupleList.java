/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Collections;


/**
 *
 * @author Miquel and Martin and Nathan
 */
public class TupleList extends ArrayList<Tuple>{

    /**
     *
     * @param tuple
     * @return flag 
     */
    @Override
    public boolean add(Tuple tuple){
        boolean flag = super.add(tuple);
        Collections.sort(this);
        return flag; 
    }
    
}
