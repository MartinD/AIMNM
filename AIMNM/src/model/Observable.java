package model;

/**
 * Created by nathandepryck on 5/05/17.
 */
public interface Observable {

    public void registerObserver(Object observer);

    public void unregisterObserver(Object observer);

}
