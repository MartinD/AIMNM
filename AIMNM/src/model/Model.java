package model;

import javafx.concurrent.Service;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by nathandepryck on 5/05/17.
 */
public class Model implements Observable, ModelInterface{

    private static Model instance;
    private ArrayList<Entity> entities;
    private ArrayList<Source> sources;
    private ArrayList<Observer> observers;
    private GeneticAlgorithm gen;
    private int runPerGen;
    private Service<Void> backgroundThread;
    
    private Model(){
        entities = new ArrayList<Entity>();
        for(int i=0; i<200; i++){
            entities.add(new Entity(1));
        }
        sources = new ArrayList<Source>();
        sources.add(new Source(3,9000, 2));
        sources.add(new Source(1,5000, 1));
        sources.add(new Source(5,15000, 3));
        observers = new ArrayList<>();
        gen = new GeneticAlgorithm(entities);
        runPerGen = 25;
    }
    
    private Model(int nbEntities, int nbSources, int runPerGen){
        entities = new ArrayList<Entity>();
        for(int i=0; i<nbEntities; i++){
            entities.add(new Entity(1));
        }
        sources = new ArrayList<Source>();
        for(int i=0; i<nbSources; i++){
            sources.add(new Source());
        }
        gen = new GeneticAlgorithm(entities);
        this.runPerGen = runPerGen;
    }

    public void setSources(int nbSources){
        sources.clear();
        Source source;
        Random rPrice = new Random();
        Random rContent = new Random();
        for(int i=0; i<nbSources; i++){
            source = new Source();
            source.setPrice(rPrice.nextInt(2)+1);
            int content = Math.abs(rContent.nextInt(4000)*source.getPrice()) + 20000;
            //System.out.println(content);
            source.setSourceContent(content);
            source.setMaxContent(content);
            source.setId(i+1);
            sources.add(source);
        }
    }

    public void setEntities(int nbEntities ){
        entities.clear();
        for(int i=0; i<nbEntities; i++){
            entities.add(new Entity(sources.size()));
        }
        gen.setEntities(entities);
    }

    
    public static Model getInstance(){
        if(instance == null){
            instance = new Model();
        }
        return instance;
    }
    
    public static Model getInstance(int nbEntities, int nbSources, int runPerGen){
        if(instance == null){
            instance = new Model(nbEntities, nbSources, runPerGen);
        }
        return instance;
    }
    
    public Source getSourceByNb(int value) {
        if(value != 0){
            for(Source s : sources){
                if(s.getId() == value){
                    return s;
                }
            }
        }
        return null;
    }

    @Override
    public void runOne(){
        Matrix input;
        Matrix output;
        Source s;
        for(Entity e : entities){
            input = getInput(e);
            output = e.run(input);
            e.setProvider(processValues(output)-1);
            s = getSourceByNb((int)output.getValue(0, 0));
            if(s != null){
                if(e.getEnergy()>0){
                    e.update((int)output.getValue(0, 1), s.getPrice(), s.getSourceContent());
                    s.update(e.getRealEnergyGain());
                }
                else{
                    e.update((int)output.getValue(0, 1), s.getPrice(), s.getSourceContent());
                }
            }
            else{
                e.update();
            }
        }
        setBest();
        updateScreen();

    }


    private void setBest(){
        int index =0;
        int bestScore = 0;
        int score;
        for (Entity e : entities) {
            score = getScore(e);
            if(score>bestScore){
                bestScore = score;
                index = entities.indexOf(e);
            }
            e.setTheBest(false);
        }
        entities.get(index).setTheBest(true);
    }

    private int getScore(Entity e){
        int score = (int) (200*Math.pow(e.getEnergy(),3)+(int)Math.sqrt(Math.abs(e.getCredit())));
        return score;
    }

    @Override
    public void init(int size){
        entities.clear();
        for(int i=0; i<200; i++){
            entities.add(new Entity(size));
        }
        gen.setEntities(entities);
    }

    private Matrix getInput(Entity e) {
        Matrix input = new Matrix(1, 2+2*sources.size());
        int i=2;
        input.setValue(0, 0, e.getEnergy());
        input.setValue(0, 1, e.getCredit());
        for(Source s : sources){
            input.setValue(0, i, s.getPrice());
            input.setValue(0, i+1, s.getSourceContent());
            i+=2;
        }
        return input;
    }
    
    private int processValues(Matrix m) {
        int v1 = (int)(m.getValue(0, 0)*sources.size()+0.99);
        int v2 = (int)(m.getValue(0, 1)*100);
        m.setValue(0, 0, v1);
        m.setValue(0, 1, v2);
        return v1;
    }

    @Override
    public void updateScreen(){
        for(Observer observer : observers){
            observer.update(sources, entities);
        }
    }


    @Override
    public int getRunPerGen() {
        return runPerGen;
    }

    @Override
    public GeneticAlgorithm getGen(){
        return gen;
    }

    @Override
    public ArrayList<Source> getSources(){
        return sources;
    }


    @Override
    public void registerObserver(Object observer) {
        observers.add((Observer) observer);
    }

    @Override
    public void unregisterObserver(Object observer) {
        observers.remove((Observer) observer);
    }

}
