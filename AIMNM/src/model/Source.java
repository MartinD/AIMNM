/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Miquel
 */
public class Source {
    
    private int id;
    private int price;
    private int sourceContent;
    private int maxContent;
    
    public Source(){
        this.price = 1;
        this.sourceContent = 12000;
        this.maxContent = 12000;
        this.id = 1;
    }
    
    public Source(int price, int sourceContent, int id){
        this.price = price;
        this.sourceContent = sourceContent;
        this.maxContent = sourceContent;
        this.id = id;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }
    
    /**
     * @return the sourceContent
     */
    public int getSourceContent() {
        return sourceContent;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @param sourceContent the sourceContent to set
     */
    public void setSourceContent(int sourceContent) {
        this.sourceContent = sourceContent;
    }

    public void setMaxContent(int maxContent){
        this.maxContent = maxContent;
    }

    public int getMaxContent(){
        return maxContent;
    }
    
    public void update(int amountCollected){
        setSourceContent(getSourceContent() - amountCollected);
    }

 
}
