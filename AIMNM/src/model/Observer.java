package model;

import javafx.scene.shape.Shape;

import java.util.ArrayList;

/**
 * Created by nathandepryck on 5/05/17.
 */
public interface Observer {

    public void update(ArrayList<Source> sources, ArrayList<Entity> entities);
}
