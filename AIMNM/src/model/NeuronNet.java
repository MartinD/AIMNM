/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Miquel
 */
public class NeuronNet {
    
    private final int nbHiddenLayers;
    private ArrayList<Matrix> weights;
    
    public NeuronNet(int nbInputs, int nbOutputs, int nbHiddenLayers, int neuronPerHiddenLayer){
        weights = new ArrayList<Matrix>();
        this.nbHiddenLayers = nbHiddenLayers;
        if(nbHiddenLayers == 0){
            weights.add(Matrix.getRandomMatrix(nbInputs, nbOutputs, -nbInputs, nbInputs));
        }
        else if(nbHiddenLayers >= 1){
            weights.add(Matrix.getRandomMatrix(nbInputs, neuronPerHiddenLayer, -nbInputs, nbInputs));
            for(int i=1; i<nbHiddenLayers; i++){
                weights.add(Matrix.getRandomMatrix(neuronPerHiddenLayer, neuronPerHiddenLayer, -nbInputs, nbInputs));
            }
            weights.add(Matrix.getRandomMatrix(neuronPerHiddenLayer, nbOutputs, -nbInputs, nbInputs));
        }
    }
    
    public NeuronNet(int nbInputs, int nbOutputs){
        this.nbHiddenLayers = 0;
        weights.add(Matrix.getRandomMatrix(nbInputs, nbOutputs, -nbInputs, nbInputs));
    }
    
    private float sigmoid(float x){
        return (float) (1/(1+Math.exp(-x)));
    }
    
    private Matrix sigmoid(Matrix m){
        for(int i=0; i<m.getRows(); i++){
            for(int j=0; j<m.getColumns(); j++){
                m.setValue(i, j, sigmoid(m.getValue(i, j)));
            }
        }
        return m;
    }
    
    public Matrix runForward(Matrix input){
        Matrix output = sigmoid(Matrix.multiply(input, weights.get(0)));
        for(int i=1; i<nbHiddenLayers+1; i++){
            output = sigmoid(Matrix.multiply(output, weights.get(i)));
        }
        return output;
    }


    public ArrayList<Matrix> getWeights() {
        return weights;
    }

    /**
     * @param weights the weights to set
     */
    public void setWeights(ArrayList<Matrix> weights) {
        this.weights = weights;
    }
}
