package model;

/**
 *
 * @author Miquel
 */
public class Matrix {
    
    private float[][] coeff = null;
    
    //Constructors
    
    public Matrix(int i, int j) {
        this.setLength(i, j);
    }

    public Matrix() {
        this(0, 0);
    }

    public Matrix(float[][] mat) {
        this.coeff = mat;
    }

    //Setters
    
    public void setMatrix(float[][] mat) {
        this.coeff = mat;
    }

    public void setValue(int i, int j, float value) {
        this.coeff[i][j] = value;
    }

    public void setRow(int i, float[] row){
        this.coeff[i] = row;
    }
    
    //Getters
    
    public float[][] getMatrix() {
        return this.coeff;
    }

    public int getRows() {
        return this.coeff.length;
    }

    public int getColumns() {
        return this.coeff[0].length;
    }

    public float getValue(int i, int j) {
        return this.coeff[i][j];
    }
    
    public float[] getRow(int i){
        return this.coeff[i];
    }
    
    //Methods
    
    private void setLength(int i, int j) {
        this.coeff = new float[i][j];
    }
    
    static public Matrix multiply(Matrix m1, Matrix m2){
        Matrix m = new Matrix(m1.getRows(), m2.getColumns());
        float sum;
        for(int i=0; i<m1.getRows(); i++){
            for(int j=0; j<m2.getColumns(); j++){
                sum = 0;
                for(int k=0; k<m1.getColumns(); k++){
                    sum += m1.getValue(i, k)*m2.getValue(k, j);
                }
                m.setValue(i, j, sum);
            }
        }
        return m;
    }
    
    static private float randRange(float min, float max) {
        return min + (float)Math.random() * (max - min);
    }
    
    static public Matrix getRandomMatrix(int i, int j, float min, float max){
        Matrix m = new Matrix(i, j);
        for(int k=0; k<i; k++){
            for(int l=0; l<j; l++){
                m.setValue(k, l, Matrix.randRange(min, max));
            }
        }   
        return m;
    }
    
    static public void mixMatrices(Matrix m1, Matrix m2){
        int separator = m1.getRows();
        float temp[];
        for(int i = 0; i<separator/2; i++){
            temp = m1.getRow(i);
            m1.setRow(i, m2.getRow(separator-1-i));
            m2.setRow(separator-1-i, temp);
        }
    }
    
    @Override
    public String toString(){
        String s = "";
        for(int i=0; i<this.getRows(); i++){
            for(int j=0; j<this.getColumns(); j++){
                s += this.getValue(i, j)+" ";
            }
            s += "\n";
        }
        return s;
    }


}
