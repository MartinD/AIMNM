/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Miquel and Martin and Nathan
 */
public class Tuple implements Comparable{
    
    private Entity ent;
    private Integer score;
    
    public Tuple(Entity ent, Integer score){
        this.ent = ent;
        this.score = score;
    }

    /**
     * @return the ent
     */
    public Entity getEnt() {
        return ent;
    }

    /**
     * @return the score
     */
    public Integer getScore() {
        return score;
    }

    /**
     * @param ent the ent to set
     */
    public void setEnt(Entity ent) {
        this.ent = ent;
    }

    /**
     * @param score the score to set
     */
    public void setScore(Integer score) {
        this.score = score;
    }


    @Override
    public int compareTo(Object o) {
        Tuple tuple = (Tuple) o;
        return score.compareTo(tuple.getScore());
    }
}
