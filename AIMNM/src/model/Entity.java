/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Miquel
 */
public class Entity {
    
    private int energy;
    private int credit;
    private boolean buy;
    private int saveMalus;
    private int realEnergyGain;
    private int provider;
    private int maxCredit = 200;

    private boolean isTheBest = false;

    private NeuronNet neuronNet;
    
    public Entity(int input){
        this.energy = 100;
        this.credit = maxCredit;
        this.buy = false;
        this.saveMalus = 0;
        this.neuronNet = new NeuronNet(4+2*(input-1), 2, 2, 5);
    }
    
    public Entity(int energy, int credit){
        this.energy = energy;
        this.credit = credit;
        this.saveMalus = 0;
        this.neuronNet = new NeuronNet(4, 2, 1, 5);
    }

    /**
     * @return the energy
     */
    public int getEnergy() {
        return energy;
    }

    /**
     * @return the credit
     */
    public int getCredit() {
        return credit;
    }

    /**
     * @return the saveMalus
     */
    public int getSaveMalus() {
        return saveMalus;
    }

    /**
     * @return the realEnergyGain
     */
    public int getRealEnergyGain() {
        return realEnergyGain;
    }

    /**
     * @param energy the energy to set
     */
    public void setEnergy(int energy) {
        this.energy = energy;
    }

    /**
     * @param credit the credit to set
     */
    public void setCredit(int credit) {
        if(credit < 0){
            this.credit = 0;
            buy = false;
        }
        else{
            buy = credit < this.credit;
            this.credit = credit;
        }

    }

    /**
     * @param saveMalus the saveMalus to set
     */
    public void setSaveMalus(int saveMalus) {
        this.saveMalus = saveMalus;
    }

    /**
     * @param realEnergyGain the realEnergyGain to set
     */
    public void setRealEnergyGain(int realEnergyGain) {
        this.realEnergyGain = realEnergyGain;
        if(this.realEnergyGain <= 0)
            this.realEnergyGain = 0;
    }

    
    public void update(int energyGain, int sourcePrice, int sourceContent, int energyUsed){
        setRealEnergyGain(energyGain);
        int priceToPay = getRealEnergyGain()*sourcePrice;
        if(priceToPay > getCredit()){
            setRealEnergyGain(getRealEnergyGain() - (priceToPay - getCredit()) / sourcePrice);
        }
        setCredit(getCredit() - priceToPay);
        if(getRealEnergyGain() > sourceContent){
            setRealEnergyGain(sourceContent);
        }
        if(getEnergy() > 0){
            setEnergy(getEnergy() + getRealEnergyGain());
        }
        if(getEnergy() > 100){
            setEnergy(100);
        }
        setEnergy(getEnergy() - energyUsed);
    }
    
    public void update(int energyGain, int sourcePrice, int sourceContent){
        setRealEnergyGain(energyGain);
        int priceToPay = getRealEnergyGain()*sourcePrice;
        if(priceToPay > getCredit()){
            setRealEnergyGain(getRealEnergyGain() - (priceToPay - getCredit()) / sourcePrice);
        }
        setCredit(getCredit() - priceToPay);
        if(getRealEnergyGain() > sourceContent){
            setRealEnergyGain(sourceContent);
        }
        if(getEnergy() > 0){
            setEnergy(getEnergy() + getRealEnergyGain());
        }
        if(getEnergy() > 100){
            setEnergy(100);
        }
        setEnergy(getEnergy() - 5);

    }
    
    public void update(){
        setEnergy(getEnergy() - 5);
    }
    
    public void update(int energyUsed){
        setEnergy(getEnergy() - energyUsed);
    }
    
    public void refundCredit(){
        if(getCredit() < 0){
            setSaveMalus(getSaveMalus() + getCredit());
            setCredit(0);
        }
        setCredit(getCredit() + 100);
    }
    
    public void refundCredit(int x){
        if(getCredit() < 0){
            setSaveMalus(getSaveMalus() + getCredit());
            setCredit(0);
        }
        setCredit(getCredit() + x);
    }

    public Matrix run(Matrix input){
        return this.neuronNet.runForward(input);
    }
    
    public NeuronNet getNeuronNet() {
        return neuronNet;
    }

    public boolean isBuy() {
        return buy;
    }


    public int getProvider() {
        return provider;
    }

    public void setProvider(int provider) {
        this.provider = provider;
    }

    public int getMaxCredit() {
        return maxCredit;
    }

    public boolean isTheBest() {
        return isTheBest;
    }

    public void setTheBest(boolean theBest) {
        isTheBest = theBest;
    }

}
