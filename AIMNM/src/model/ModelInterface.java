package model;

import java.util.ArrayList;

/**
 * Created by user on 18/05/17.
 */
public interface ModelInterface{

    public void updateScreen();

    public void setSources(int nbSources);

    public void setEntities(int nbEntities);

    public void runOne();

    public int getRunPerGen();

    public GeneticAlgorithm getGen();

    public ArrayList<Source> getSources();

    public void init(int size);


}
