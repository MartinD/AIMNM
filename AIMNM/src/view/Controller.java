package view;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import model.*;

import java.util.ArrayList;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controller implements Observer{

    private ModelInterface model;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Pane pane;
    @FXML
    private Button start;
    @FXML
    private Button pause;
    @FXML
    private Text text;
    @FXML
    private Text previousGen;
    @FXML
    private Text bestGen;
    @FXML
    private Text numBestGen;
    @FXML
    private Text numGen;
    @FXML
    private Text rapidity;
    @FXML
    private Slider slider;
    @FXML
    private TextField numberEntities;
    @FXML
    private TextField numberSource;
    @FXML
    private Text Entity;
    @FXML
    private Text Source;
    @FXML
    private ButtonBar bar;





    private BufferedWriter bufferedWriter;
    private Thread thread;
    private int previousScore = 0;
    private int bestScore = 0;
    private int alive = 0;
    private int numScore = 0;
    private int stopCounter = 0;
    private int bestScoreGen = 0;
    private boolean testMode = true;
    private int nbTest = 0;

    private double halfScreenX;
    private double halfScreenY;

    @FXML
    private void initialize(){
        previousScore = 0;
        bestScore = 0;
        alive = 0;
        numScore = 0;
        slider.setMin(100);
        slider.setMax(1000);
        try {
            bufferedWriter = new BufferedWriter(new FileWriter("data.txt", true));
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    @FXML
    private void onStart(){
        if(start.getText().equals("Start")) {
            initialize();
            if (!Objects.equals(numberSource.getText(), "") && !Objects.equals(numberEntities.getText(), "")) {
                int nbrSources = Integer.valueOf(numberSource.getText());
                model.setSources(nbrSources);
                int nbrEntities = Integer.valueOf(numberEntities.getText());
                model.setEntities(nbrEntities);
                thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while ((testMode && nbTest < 10) || !testMode) {
                            numScore++;
                            for (Source s : model.getSources()) {
                                s.setSourceContent(s.getMaxContent());
                            }
                            for (int i = 0; i < model.getRunPerGen(); i++) {
                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        model.runOne();
                                    }
                                });
                                try {
                                    thread.sleep((int) slider.getValue());
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            previousScore = alive;
                            if (previousScore >= bestScore) {
                                bestScore = previousScore;
                                bestScoreGen = numScore;
                                stopCounter = 0;
                            } else {
                                stopCounter++;
                            }
                            if (stopCounter >= 50 && testMode) {
                                try {
                                    getBufferedWriter().write(bestScore + " " + bestScoreGen + "\n");
                                } catch (IOException ex) {
                                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                reset();
                                nbTest++;
                            }
                            double nbEnt = Double.valueOf(numberEntities.getText());
                            model.getGen().setProp((nbEnt - alive)/nbEnt);
                            model.getGen().nextGeneration();
                        }
                    }

                    private void reset() {
                        previousScore = 0;
                        bestScore = 0;
                        alive = 0;
                        numScore = 0;
                        model.init(model.getSources().size());
                    }
                });
                thread.start();
                start.setText("Reset");
            } else {
                Text text = new Text(pane.getWidth()/2, pane.getHeight()/2, "Fill in the values!!");
                text.setFill(Color.RED);
                pane.getChildren().clear();
                pane.getChildren().add(text);
            }
        }else if (start.getText().equals("Reset")) {
                thread.stop();
                model.init(1);
                model.updateScreen();
                start.setText("Start");
        }


    }

    @FXML
    private void onPause(){
        if(!Objects.equals(pause.getText(), "Resume")) {
            pause.setText("Resume");
            thread.suspend();
        }
        else{
            pause.setText("Pause");
            thread.resume();
        }
    }

    @Override
    public void update(ArrayList<Source> sources, ArrayList<Entity> entities) {
        halfScreenX = pane.getWidth()/2;
        halfScreenY = pane.getHeight()/2;
        pane.getChildren().clear();
        anchorPane.getChildren().clear();
        alive = 0;
        updateSources(sources);
        updateEntities(entities, sources.size());
        updateAnchorPane();
    }

    private void updateSources(ArrayList<Source> sources){
        Circle component;
        Text t;
        for (Source source : sources){
            component = new Circle(50*source.getSourceContent()/source.getMaxContent());
            t = new Text("Price: "+source.getPrice()+"\nContent: " + source.getSourceContent());
            double posX = halfScreenX + 125 * Math.cos((2 * Math.PI * sources.indexOf(source)) / sources.size());
            double posY = halfScreenY + 125 * Math.sin((2 * Math.PI * sources.indexOf(source)) / sources.size());
            if(sources.size() > 1) {
                component.setCenterX(posX);
                component.setCenterY(posY);
                t.setX(posX - 25);
                t.setY(posY);
                t.setFill(Color.BLACK);
            }
            else{
                t.setX(halfScreenX);
                t.setY(halfScreenY);
                component.setCenterX(halfScreenX);
                component.setCenterY(halfScreenY);
            }
            component.setFill(Color.web("#3e3ab7"));
            pane.getChildren().add(component);
            pane.getChildren().add(t);
        }
    }

    private void updateEntities(ArrayList<Entity> entities, int sourcesSize){
        Circle component;
        Line money;
        Line buy;
        for (Entity entity : entities){
            double posX = halfScreenX+400*Math.cos((2*Math.PI*entities.indexOf(entity))/entities.size());
            double posY = halfScreenY+400*Math.sin((2*Math.PI*entities.indexOf(entity))/entities.size());
            if(entity.getCredit() > 0) {
                money = new Line(halfScreenX + (entity.getCredit()/3 + 420) * Math.cos((2 * Math.PI * entities.indexOf(entity)) / entities.size()),
                        halfScreenY + (420 + entity.getCredit()/3) * Math.sin((2 * Math.PI * entities.indexOf(entity)) / entities.size()),
                        halfScreenX + 420 * Math.cos((2 * Math.PI * entities.indexOf(entity)) / entities.size()),
                        halfScreenY + 420 * Math.sin((2 * Math.PI * entities.indexOf(entity)) / entities.size()));
                money.setStroke(Color.YELLOW);
                money.setStrokeWidth(5);
                pane.getChildren().add(money);
            }
            if(entity.isBuy()){
                if(sourcesSize > 1) {
                    buy = new Line(posX,
                            posY,
                            halfScreenX + 125 * Math.cos((2 * Math.PI * entity.getProvider()) / sourcesSize),
                            halfScreenY + 125 * Math.sin((2 * Math.PI * entity.getProvider()) / sourcesSize));
                }
                else {
                    buy = new Line(posX,
                            posY,
                            halfScreenX,
                            halfScreenY);
                }
                pane.getChildren().add(buy);
            }
            if(entity.getEnergy() >= 0) {
                alive++;
                if(entity.isTheBest()){
                    component = new Circle(5 * entity.getEnergy() / 100,Color.GREEN);
                }
                else{
                    component = new Circle(5 * entity.getEnergy() / 100,Color.BLACK);
                }
            }
            else {
                component = new Circle(5, Color.RED);
                component.setOpacity(0.2);
            }
            component.setCenterX(posX);
            component.setCenterY(posY);

            pane.getChildren().add(component);
        }
    }

    private void updateAnchorPane(){
        numGen.setText("Generation number: " + numScore);
        text.setText("Entities alive: " + alive);
        previousGen.setText("Previous generation: " + previousScore);
        bestGen.setText("Best generation: " + bestScore);
        numBestGen.setText("Best generation number: " + bestScoreGen);
        anchorPane.getChildren().add(numGen);
        anchorPane.getChildren().add(text);
        anchorPane.getChildren().add(previousGen);
        anchorPane.getChildren().add(bestGen);
        anchorPane.getChildren().add(numBestGen);
        anchorPane.getChildren().add(slider);
        anchorPane.getChildren().add(rapidity);
        anchorPane.getChildren().add(Entity);
        anchorPane.getChildren().add(numberEntities);
        anchorPane.getChildren().add(Source);
        anchorPane.getChildren().add(numberSource);
        anchorPane.getChildren().add(bar);
    }

    public void setModel(ModelInterface model) {
        this.model = model;
        Observable observable = (Observable) model;
        observable.registerObserver(this);
    }

    public Thread getThread() {
        return thread;
    }

    /**
     * @return the bufferedWriter
     */
    public BufferedWriter getBufferedWriter() {
        return bufferedWriter;
    }
}
